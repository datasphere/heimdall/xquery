module namespace m = 'http://heimdall.huma-num.fr/csv';
import module namespace heimdall = 'http://heimdall.huma-num.fr' at 'heimdall.xqm';
import module namespace map = 'http://heimdall.huma-num.fr/util' at 'util.xqm';

(:~
 : Liste tous les records de la base de données décrite par `$db`.
 : `$db` doit décrire un fichier au format CSV.
 : <br>
 : Le paramètre `$db` est une map décrivant la ressource à interroger.
 : Cette map doit obligatoirement contenir les clés :
 : <ul>
 : <li>`url` valuée avec un `xs:string` représentant l'URL du fichier CSV</li>
 : <li>`format` valuée à `csv`.</li>
 : <ul>
 : La map `$db` peut aussi contenir les clés optionnelles suivantes :
 : <ul>
 : <li>`separator` valuée avec un `xs:string` qui sert de séparateur
 :     entre les valeurs d'une même ligne.
 : </li>
 : <li>`quotes` valuée à `yes` si les valeurs sont délimitées par des quotes.
 : </li>
 : <li>`header` valuée une valeur vraie si la première ligne du fichier
 :     comporte les noms de colonnes.
 : </li>
 : </ul>
 : Si `$db("url")` est inaccessible ou n'est pas un fichier CSV,
 : le comportement de `getDatabaseItems` n'est pas spécifié.
 : @param $db description d'une base de données (voir description)
 : @return liste d'éléments `<item/>`
 : @see https://docs.basex.org/main/CSV_Functions
 :)
declare function m:getDatabaseItems(
    $db as map(*)
    ) as element()* {
  let $text := file:read-text($db('url'))
  let $csv := csv:parse($text, map {
    'separator': map:get($db, 'separator', ','),
    'quotes':    map:get($db, 'quotes', 'yes'),
    'header': true()
    })
  for $row in $csv/csv/*[node-name(.) eq xs:QName('record')]
    return <item>{
      for $column in $row/*
        return <metadata pid="{$column/name()}">{$column/text()}</metadata>
    }</item>
};

(:~
 :)
declare function m:serialize(
    $items as element()*,
    $options as map(*)?
  ) as xs:string {
  let $header := $options('header')
  let $content :=
  <csv>{
  for $item in $items
    return
    <record>{
      for $column in $header
        return
        element {$column} { heimdall:getValueById($item, $column) }
    }</record>
  }</csv>
  return
  serialize($content, map {
    'method': 'csv',
    'csv': map {
      'separator': map:get($options, 'separator', ','),
      'format':    map:get($options, 'quotes', 'direct'),
      'lax':    map:get($options, 'quotes', 'yes'),
      'quotes':    map:get($options, 'quotes', 'yes'),
      'backslashes':    map:get($options, 'quotes', 'no'),
      'header': true()
    }
  })
};
