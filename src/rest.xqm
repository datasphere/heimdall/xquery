module namespace m = 'http://heimdall.huma-num.fr/rest';

(:~
 : Liste tous les éléments présents derrière la collection mise à disposition
 : par une API REST décrite par `$db`.
 : Le retour est une liste d'éléments `<item/>`.
 : Chaque élément `<item/>` contient 0..N éléments `<metadata/>` :
 : ```
 : <item>
 :   <metadata id="id1">value1</metadata>
 :   <metadata id="id2">value2</metadata>
 :   ...
 :   <metadata id="idN">valueN</metadata>
 : </item>
 : ```
 : Le travail de cette fonction est d'aboutir à cette organisation en éléments
 : `<item/>` et `<metadata/>` en gommant les spécificités de l'API REST dont
 : la description d'une ressource est passée en paramètre.
 : <br>
 : Le paramètre `$db` est une map décrivant la resource à interroger.
 : Cette map doit obligatoirement contenir les clés suivantes
 : (attention, les clés et leur valeurs sont sensibles à la casse) :
 : <ul>
 : <li>`url` valuée avec un `xs:string` représentant l'URL de la resource</li>
 : <li>`format` valuée à `api:json`.</li>
 : <ul>
 : Si `$db("url")` désigne une base inexistante ou dans un format qui n'est pas
 : REST, le comportement de `getDatabaseItems` n'est pas spécifié.
 : @param $db description d'une base de données (voir description)
 : @return liste d'éléments `<item/>`
 : @see https://docs.basex.org/main/HTTP_Client_Functions
 : @see https://fr.wikipedia.org/wiki/Representational_state_transfer
 :)
declare function m:getDatabaseItems(
    $db as map(*)
    ) as element()* {
  let $collections := m:getCollections($db('url'))
  return
  for $object in $collections/*
    return
    <item>{
      for $attribute in $object/*
        let $content := 
          if (fn:exists($attribute/*))
            then $attribute/*
            else $attribute/text()
        return
        <metadata pid="{$attribute/name()}">{$content}</metadata>
    }</item>
};

(:~
 : @param $url URL d'une collection de ressources dans une API REST
 : @return éléments XML de type="array"
 : @see https://docs.basex.org/main/HTTP_Client_Functions
 :)
declare function m:getCollections(
    $url as xs:string
    ) as element()* {
  let $request := 
    <http:request method='GET' href='{$url}'>
      <http:header name='Accept' value='application/json' />
    </http:request>
  let $response := http:send-request($request)
  return $response/json/*[@type='array']
};
