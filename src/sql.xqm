module namespace m = 'http://heimdall.huma-num.fr/sql';

(:~
 : Liste tous les éléments de la base de données décrite par `$db`.
 : `$db` doit être disponible sur un serveur SQL accessible depuis localhost.
 : Le travail de cette fonction est d'aboutir à cette organisation en éléments
 : `<item/>` et `<metadata/>` en gommant les spécificités du schema de la
 : base de données considérée.
 : Chaque ligne de chacune des tables de cette base de données est représenté
 : comme un élément `<item/>` ; la valeur de chaque cellule de la table est
 : un élément `<metadata/>` d'identifiant égal au nom de la colonne considérée.
 : Cette fonction est en fait le comportement de `getRows`, appliqué
 : à toutes les tables de `useTables`.
 : <br>
 : Le paramètre `$db` est une map décrivant la ressource à interroger.
 : Cette map doit obligatoirement contenir les clés :
 : <ul>
 : <li>`url` valuée avec un `xs:string` représentant
 :     l'URL de la base SQL ou du serveur SQL l'hébergeant.
 : </li>
 : <li>`database` valuée avec un `xs:string` représentant le nom de la
 :      base de données dont on veut lister le contenu.
 : </li>
 : <li>`format` valuée à `sql:mysql` ou à `sql:mariadb`.</li>
 : </ul>
 : La map `$db` peut aussi contenir les clés optionnelles suivantes :
 : <ul>
 : <li>`tables` valuée avec une sequence de `xs:string` énumérant les tables
 :      dont le contenu doit être listé. Toute table présente dans la base de
 :      données `database` mais absente de `table` sera ignorée.
 : </li>
 : <li>`login` valuée avec un `xs:string` étant l'identifiant d'un utilisateur
 :     disposant de droits en lecture sur les `tables` de la `database`.
 : </li>
 : <li>`password` valuée avec un `xs:string` étant le mot de passe de
 :     l'utilisateur décrit par la clé `login`.
 : </li>
 : </ul>
 : Si `$db("url")` est inaccessible ou n'est pas une base de données SQL dans
 : un format correspondant à `$db("format")`, le comportement de
 : `getDatabaseItems` n'est pas spécifié.
 : @param $db description d'une base de données (voir description)
 : @return liste d'éléments `<item/>`
 : @see https://docs.basex.org/main/SQL_Functions
 : @see getRows
 :)
declare function m:getDatabaseItems(
    $db as map(*)
    ) as element()* {
  let $format := $db('format')
  return
  if ($format = 'sql:mysql' or $format = 'sql:mariadb') then (
    sql:init('org.mariadb.jdbc.Driver'),
    let $conn := sql:connect(concat('jdbc:', $db('url')), $db('login'), $db('password'))
    let $database := $db('database')
    let $tables := m:useTables($conn, $database, $db('tables'))
    let $items := m:getItems($conn, $database, $tables)
    let $nothing := sql:close($conn)
    return $items
  ) else (
    fn:error(
      xs:QName('FORMAT_NOT_SUPPORTED'),
      concat("Unsupported database format '",$format,"'.")
      )
  )
};

(:~
 : Liste les bases de données disponibles à travers une connexion ouverte
 : vers un serveur SQL. Cette connexion est typiquement obtenue par un appel à
 : la fonction `sql:connect`.
 : @param $connection identifiant de connexion
 : @return liste des noms des bases de données
 : @see https://docs.basex.org/main/SQL_Functions#sql:connect
 :)
declare function m:getDatabases(
    $connection as xs:anyURI
    ) as xs:string* {
  let $databases := sql:execute($connection, 'SHOW DATABASES;')
  return $databases/sql:column/text()
};

(:~
 : Liste les tables de la base de données SQL `$dbname` à utiliser.
 : <ul>
 : <li>si la séquence `$tables` est non-vide, cette fonction se contente de
 :     renvoyer cette séquence sans modification
 : </li>
 : <li>si la séquence `$tables` est laissée vide, cette fonction renvoie
       toutes les tables effectivement disponibles dans la base `$dbname`.
 : </li>
 : </ul>
 : @param $connection identifiant de connexion
 : @param $dbname nom d'une base de données
 : @param $tables à laisser vide si on veut toutes les tables
 : @return liste des noms des tables
 :)
declare function m:useTables(
    $connection as xs:anyURI,
    $dbname as xs:string,
    $tables as xs:string*
    ) as xs:string* {
  if (empty($tables)) then (
    let $init := sql:execute($connection, concat('USE `',$dbname,'`;'))
    let $tables := sql:execute($connection, 'SHOW TABLES;')
    return $tables/sql:column/text()
  ) else (
    $tables
  )
};

(:~
 : Liste les données présentes dans la table nommée `$tablename`
 : présente dans la base de données nommée `$dbname`.
 : Le resultat est une liste d'éléments XML `<item/>` ; chaque
 : valeur de chaque colonne de la table `$tablename` est un élement
 : XML `<metadata/>`.
 : @param $connection identifiant de connexion
 : @param $dbname nom d'une base de données
 : @param $tablename nom d'une table
 : @return liste des éléments de `$dbname.$tablename`
 :)
declare function m:getRows(
    $connection as xs:anyURI,
    $dbname as xs:string,
    $tablename as xs:string
    ) as element()* {
  let $init := sql:execute($connection, concat('USE `',$dbname,'`;'))
  let $rows := sql:execute($connection, concat('SELECT * FROM `',$tablename,'`;'))
  for $row in $rows
    return <item>{
      <metadata pid="sql:table">{$tablename}</metadata>,
      for $column in $row/sql:column
        return <metadata pid="{$tablename}:{$column/@name}">
          {$column/text()}
        </metadata>
    }</item>
};

(:~
 : Liste toutes les données présentes dans les tables listées dans la séquence
 : `$tables` de la base de données nommée `$dbname`.
 : Chaque ligne de chacune des tables de cette base de données est représenté
 : comme un élément XML `<item/>`.
 : Cette fonction est en fait le comportement de `getRows`, appliqué
 : à toutes les tables de `$dbname`.
 : @param $connection identifiant de connexion
 : @param $dbname nom d'une base de données
 : @param $tables liste des tables dont les éléments sont à lister
 : @return liste des éléments contenues dans les tables `$tables` de `$dbname`
 : @see getRows
 :)
declare function m:getItems(
    $connection as xs:anyURI,
    $dbname as xs:string,
    $tables as xs:string*
    ) as element()* {
  let $init := sql:execute($connection, concat('USE `',$dbname,'`;'))
  for $table in $tables
    return m:getRows($connection, $dbname, $table)
};
