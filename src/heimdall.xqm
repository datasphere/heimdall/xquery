(:~
 : Module d'interrogation générique des fichiers HML (=Heurist xML).
 : <br/>
 : Certaines des fonctions proposées par ce module (<code>getDatabaseItems</code>,
 : <code>getItems</code> et <code>getItem</code>) prennent en premier paramètre
 : <code>$db</code>, une map décrivant la base de données à interroger.
 : Cette map doit obligatoirement contenir les clés suivantes
 : (attention, les clés et leur valeurs sont sensibles à la casse) :
 : <ul>
 : <li><code>url</code> valuée avec un <code>xs:string</code> représentant
 :     l'URL de la base de données, c'est à dire le chemin local ou distant
 :     vers la base de données à interroger.
 : </li>
 : <li><code>format</code> valuée à l'un des <code>xs:string</code> suivants,
       représentant le format de la base de données,
       c'est à dire son architecture « physique » :
 :   <ul>
 :   <li><code>xml:heurist</code> pour un fichier XML exporté d'Heurist</li>
 :   <li><code>api:json</code> pour une collection mise à disposition par une API REST</li>
 :   <li><code>csv</code> pour une base tenant dans un fichier CSV</li>
 :   </ul>
 : </li>
 : </ul>
 : Si <code>$db("url")</code> désigne une base inexistante ou dans un format
 : non supporté, le comportement des fonctions sus-nommées n'est pas spécifié.
 : @author Guillaume Porte
 : @author Elsa Van Kote
 : @author Régis Witz
 : @see https://gitlab.huma-num.fr/datasphere/heimdall
 :)
module namespace heimdall = 'http://heimdall.huma-num.fr';
import module namespace sql = 'http://heimdall.huma-num.fr/sql' at 'sql.xqm';
import module namespace hml = 'http://heimdall.huma-num.fr/hml' at 'hml.xqm';
import module namespace csv = 'http://heimdall.huma-num.fr/csv' at 'csv.xqm';
import module namespace json = 'http://heimdall.huma-num.fr/json' at 'json.xqm';
import module namespace rest = 'http://heimdall.huma-num.fr/rest' at 'rest.xqm';
import module namespace hera = 'http://heimdall.huma-num.fr/hera' at 'hera.xqm';
import module namespace u = 'http://heimdall.huma-num.fr/util' at 'util.xqm';


(:~
 : Liste tous les items présents dans la base de données <code>$db</code>.
 : Le retour est une liste d'éléments <code>item</code>.
 : Chaque élément <code>item</code> contient 0..N éléments <code>metadata</code>
 : @param <code>$db</code> description d'une base de données (voir description du module)
 : @return liste d'éléments <code>item</code>
 : @example <ul>
 : <li>Lister toutes les lignes d'un fichier CSV standard <code>pets.csv</code> :
 :   <br/>
 :   <pre>
 : heimdall:getDatabaseItems(map {
 :   'url': 'path/to/pets.csv',
 :   'separator': ',',
 :   'format': 'csv'
 : })
 :   </pre>
 : </li>
 : <li>Lister tous les enregistrements de deux tables d'une base MySQL:
 :   <br/>
 :   <pre>
 : heimdall:getDatabaseItems(map {
 :   'url': 'mysql://localhost',
 :   'database': 'your_db_name_here',
 :   'tables': ('my_favourite_table', 'another_table_I_want'),
 :   'login': 'proot',
 :   'password': 'your_password_here_but_dont_commit_it_plz',
 :   'format': 'sql:mysql'
 : })
 :   </pre>
 : </li>
 : <li>Lister tous les éléments <code>record</code> d'un fichier XML issu d'Heurist :
 :   <br/>
 :   <pre>
 : heimdall:getDatabaseItems(map {
 :   'url': 'path/to/ExportXXXXX.xml',
 :   'format': 'xml:heurist'
 : })
 :   </pre>
 : </li>
 : </ul>
 :)
declare function heimdall:getDatabaseItems(
    $db as map(*)
    ) as element()* {
  let $format := $db('format')
  return
  if ($format = 'sql:mysql') then (
    sql:getDatabaseItems($db)
  ) else
  if ($format = 'xml:hera') then (
    hera:getDatabaseItems($db)
  ) else
  if ($format = 'xml:heurist') then (
    hml:getDatabaseItems($db)
  ) else
  if ($format = 'csv') then (
    csv:getDatabaseItems($db)
  ) else
  if ($format = 'json') then (
    json:getDatabaseItems($db)
  ) else
  if ($format = 'api:json') then (
    rest:getDatabaseItems($db)
  ) else (
    fn:error(
      xs:QName('FODC0002'),
      concat("Unsupported database format '",$format,"'.")
      )
  )
};

(:~
 : Liste les éléments <code>item</code> contenant au moins un élément <code>metadata</code>
 : dont l'attribut `pid` vaut <code>$metadata</code> et dont la valeur est <code>$value</code>.
 : Par exemple, `getItem($items,"status","valid")` liste tous les items de la liste
 : <code>$items</code> qui ont une métadonnée dont l'identifiant de propriété est `status` et valant `valid`.
 : <code>$metadata</code> et <code>$value</code> sont sensibles à la casse.
 : @param <code>$items</code> liste d'éléments <code>item</code>
 : (telle que retournée par <a href="#heimdall:getDatabaseItems">getDatabaseItems</a>)
 : @param <code>$metadata</code> pid de métadonnée
 : @param <code>$value</code> valeur de métadonnée
 :        (<code>*:metadata/text() = $value</code>)
 : @return liste d'éléments <code>item</code>
 : @example <ul>
 : <li>Lister tous les éléments de la liste <code>$items</code>
 : ayant une métadonnée de nom "status" valant "valid" :
 : <br/>
 : <pre>heimdall:getItems($items,'status','valid')</pre>
 : </li>
 : </ul>
 :)
declare function heimdall:getItems(
    $items as element()*,
    $metadata as xs:string,
    $value as xs:string
    ) as element()* {
  for $item in $items
    where $item//*:metadata[@pid = $metadata and text() = $value]
    return $item
};

(:~
 : <ul>
 : <li>
 :   Renvoie un objet <code>item</code> si et seulement si il n'y a
 :   qu'un seul élément <code>item</code> dans <code>$items</code> pour qui
 :   la métadonnée <code>$metadata</code> vaut <code>$value</code> ;
 : </li><li>
 :   Renvoie <code>empty-sequence()</code> si <code>$items</code>
 :   ne contient pas d'élément <code>item</code> pour qui
 :   la métadonnée <code>$metadata</code> vaut <code>$value</code> ;
 : </li><li>
 :   Lève l'erreur <code>XPTY0004</code> si <code>$items</code> contient
 :   au moins deux éléments <code>item</code> ou davantage pour qui
 :   la métadonnée <code>$metadata</code> vaut <code>$value</code>.
 : </li>
 : </ul>
 : <code>$metadata</code> et <code>$value</code> sont sensibles à la casse.
 : @param <code>$items</code> liste d'éléments <code>item</code>
 : (retournée par <a href="#heimdall:getDatabaseItems">getDatabaseItems</a>)
 : @param <code>$metadata</code> nom de métadonnée
 : @param <code>$value</code> valeur de métadonnée
 :        (<code>*:metadata/text() = $value</code>)
 : @return un seul élément <code>item</code> (voir description)
 : @error <code>XPTY0004</code> s'il y a ambiguité / si l'élément n'est pas unique (voir description)
 : @example <ul>
 : <li>Récupérer l'unique élément de la liste <code>$items</code> ayant
 : une métadonnée dont l'identifiant de propriété est "pid" et valant "42" :
 : <br/>
 : <pre>heimdall:getItem($items,'id','42')</pre>
 : </li>
 : </ul>
 :)
declare function heimdall:getItem(
    $items as element()*,
    $metadata as xs:string,
    $value as xs:string
    ) as element()? {
  let $after := heimdall:getItems($items, $metadata, $value)
  let $count := count($after)
  return
  if ($count > 1) then (
    fn:error(
      xs:QName('XPTY0004'),
      concat("'",$metadata,"'='",$value,"' for more than 1 item (",$count,")")
      )
  ) else (
    $after
  )
};

(:~
 : Renvoie une liste de tous les éléments <code>metadata</code> d'un objet donné.
 : <br/>Cette fonction effectue une recherche profonde (<em>deep search</em>).
 : <br/>Cette fonction est pensée pour être utilisée avec un élément
 : <code>item</code>, mais recherchera des <code>metadata</code>
 : comme enfants de n'importe quel élément XML.
 : @param <code>$item</code> élément <code>item</code> (mais voir description)
 : @return liste d'éléments <code>metadata</code>
 : @example <ul>
 : <li>Lister toutes les métadonnées dont l'élément de pid "id" vaut "42"
 : présent dans liste <code>$items</code> :
 : <br/>
 : <pre>heimdall:getMetadata(getItem($items,'id','42'))</pre>
 : </li>
 : </ul>
 :)
declare function heimdall:getMetadata(
    $item as element()
    ) as element()* {
  $item//*:metadata
};

(:~
 : Renvoie une <a href="https://docs.basex.org/wiki/Map_Module">map</a> énumérant, pour chaque élément E d'une list <code>$item</code> :
 : <ul>
 : <li>comme clé, l'identifiant de propriété de chaque élément <code>metadata</code> contenue dans E ;</li>
 : <li>comme valeur, la valeur de chaque élément <code>metadata</code> contenue dans E.</li>
 : </ul>
 : Cette fonction est pensée pour E étant un élément <code>item</code>, mais
 : recherchera des éléments <code>metadata</code> comme enfants de n'importe
 : quel élément XML.
 : <br/>
 : Si <code>$item</code> ne contient aucun élément <code>metadata</code> parmi
 : ses enfants directs, la map retournée sera vide.
 : @param <code>$item</code> élément contenant des éléments <code>metadata</code> (voir description)
 : @return une map <code>{ metadataPropertyId => metadataValue }</code>
 : @see https://docs.basex.org/wiki/Map_Module
 : @example <ul>
 : <li>
 : Transformation d'un élément XML en une map de ses métadonnées :
 : <pre>
 : heimdall:getValues($item)
 : </pre>
 : </li>
 : </ul>
 :)
declare function heimdall:getValues(
    $item as element()
    ) as map(*) {
  let $metadata := heimdall:getMetadata($item)
  return
  map:merge(
    for $m in $metadata
      return map:entry($m/@pid, $m/text())
    )
};

(:~
 : Renvoie une liste de chaînes de caractères correspondant aux valeurs des
 : éléments <code>metadata</code> d'un élément <code>item</code> donné,
 : d'après l'identifiant de propriété de cet élément <code>metadata</code>.
 : <br/>
 : La liste peut contenir 0 (respectivement plusieurs) éléments,
 : selon que l'élément <code>metadata</code> est absent (respectivement répété).
 : <br/>
 : Cette fonction est pensée pour être utilisée avec un élément <code>item</code>,
 : mais recherchera des éléments <code>metadata</code> comme enfants de
 : n'importe quel élément XML.
 : @param <code>$item</code> élément <code>item</code> (mais voir description)
 : @param <code>$propertyId</code> identifiant de propriété (sensible à la casse)
 : @return valeur de métadonnée (<code>*:metadata/text() = $value</code>)
 : @example <ul>
 : <li>Lister, parmi les enfants (directs ou non) d'un élément <code>$book</code>,
 : tous les éléments <code>metadata</code> dont l'identifiant est "creator" :
 : <pre>
 : heimdall:getValueById($book, 'creator')
 : </pre>
 : </li>
 : </ul>
 :)
declare function heimdall:getValueById(
    $item as element(),
    $propertyId as xs:string
    ) as xs:string* {
  (
  for $metadata in $item//*:metadata[@pid=$propertyId]
    return $metadata/text()
  )
};

(:~
 : Renvoie une liste de chaînes de caractères correspondant aux valeurs des
 : éléments <code>metadata</code> d'un <code>item</code> donné,
 : d'après le valeur d'attribut de cet élément <code>metadata</code>.
 : <br/>
 : La liste peut contenir 0 (respectivement plusieurs) éléments,
 : selon que l'élément <code>metadata</code> est absent (respectivement répété).
 : <br/>
 : Cette fonction est pensée pour être utilisée avec un élément <code>item</code>,
 : mais recherchera des <code>metadata</code> comme enfants de
 : n'importe quel élément XML.
 : @param <code>$item</code> élément <code>item</code> (mais voir description)
 : @param <code>$attribute</code> nom d'attribut de métadonnée (sensible à la casse)
 : @param <code>$value</code> valeur d'attribut de métadonnée (sensible à la casse)
 : @return valeur de métadonnée (<code>*:metadata/text() = $value</code>)
 : @example <ul>
 : <li>Lister, parmi les enfants (directs ou non) d'un élément <code>$book</code>,
 : tous les éléments <code>metadata</code> dont le nom est "Auteur" :
 : <pre>
 : heimdall:getValueBy($book, 'name', "Auteur")
 : </pre>
 : </li>
 : </ul>
 :)
declare function heimdall:getValueBy(
    $item as element(),
    $attribute as xs:string,
    $value as xs:string
    ) as xs:string* {
  (
  for $metadata in $item//*:metadata[@*[local-name()=$attribute]=$value]
    return $metadata/text()
  )
};

(:~
 : Convertit (<em>ie.</em> exporte, sérialise, ...) une liste d'éléments
 : <code>$item</code> vers un format spécifié en paramètre.
 : @param <code>$item</code> Liste d'éléments <code>item</code>
 : @param <code>$options</code> Paramètres de la sérialization
 : @return Résultat de la conversion -il s'agit d'un simple texte,
 :  facile par exemple à écrire dans un fichier ou sur la sortie standard
 : @example <ul>
 : <li>Lecture de toutes les tables d'une base MySQL, suivie d'une conversion
 : des données vers le format CSV, mais en ne conservant que 3 colonnes
 : "id", "name" et "favourite_pet" :
 : <br/>
 : <pre>
 : let $items := heimdall:getDatabaseItems(map {
 :   'url': 'mysql://localhost',
 :   'database': 'your_db_name_here',
 :   'format': 'sql:mysql'
 : })
 : return
 : heimdall:serialize($items, map {
 :   'format': 'csv',
 :   'header': ('id','name','favourite_pet')
 : })
 : </pre>
 : <br/>
 : Évidemment, si la base SQL originale contenait des tables dénuées
 : par exemple de colonne "name" ou "favourite_pet", cet enchaînement de
 : commandes ne fait pas vraiment sens.
 : <br/>
 : Mais c'est juste un exemple, qui plus est le dernier de cette page, donc
 : vous en savez normalement assez maintenant pour implémenter les traitements
 : qui vous intéressent <em>réellement</em> ! 🌈🌉⚔️
 : </li>
 : </ul>
 :)
declare function heimdall:serialize(
    $items as element()*,
    $options as map(*)?
  ) as xs:string {
  let $format := $options('format')
  return
  if (fn:starts-with($format, 'xml')) then (
    hera:serialize($items, $options)
  ) else
  if ($format = 'csv') then (
    csv:serialize($items, $options)
  ) else
  if ($format = 'json') then (
    json:serialize($items, $options)
  ) else (
    fn:error(
      xs:QName('FODC0002'),
      concat("Unsupported database format '",$format,"'.")
      )
  )
};
