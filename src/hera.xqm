(:~
: Module d'interrogation générique des fichiers HERA.
: @see https://gitlab.huma-num.fr/datasphere/heimdall
:)
module namespace m = 'http://heimdall.huma-num.fr/hera';
import module namespace u = 'http://heimdall.huma-num.fr/util' at 'util.xqm';

declare function m:getDatabaseItems(
    $db as map(*)
    ) as element()* {
  let $items := doc($db('url'))//*:item
  return
  $items
};



declare function m:serialize(
    $items as element()*,
    $options as map(*)?
  ) as xs:string {
  let $baseurl := 'https://datasphere.huma-num.fr/hera'
  let $repourl := 'https://gitlab.huma-num.fr/datasphere/hera/schema'
  let $root_tag := u:get($options, 'root', 'hera')
  let $content := element { $root_tag } {
    attribute xsi:schemaLocation { fn:concat($repourl, '/schema.xsd') },
    <items>{$items}</items>
    }
  return serialize($content, map { 'method': 'xml' })
};
