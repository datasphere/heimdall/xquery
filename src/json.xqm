module namespace m = 'http://heimdall.huma-num.fr/json';
import module namespace heimdall = 'http://heimdall.huma-num.fr' at 'heimdall.xqm';
import module namespace map = 'http://heimdall.huma-num.fr/util' at 'util.xqm';

(:~
 : Liste tous les records de la base de données décrite par `$db`.
 : `$db` doit décrire un fichier au format JSON.
 : <br>
 : Le paramètre `$db` est une map décrivant la ressource à interroger.
 : Cette map doit obligatoirement contenir les clés :
 : <ul>
 : <li>`url` valuée avec un `xs:string` représentant l'URL du fichier JSON</li>
 : <li>`format` valuée à `json`.</li>
 : <ul>
 : La map `$db` peut aussi contenir les clés optionnelles suivantes :
 : <ul>
 : <li>`conversion`, qui a le même sens que l'option `format` du module basex JSON ;
 : </li>
 : <li>`duplicates`, qui a le même sens que l'option `duplicates` du module basex JSON.
 : </li>
 : </ul>
 : Si `$db("url")` est inaccessible ou n'est pas un fichier JSON,
 : le comportement de `getDatabaseItems` n'est pas spécifié.
 : @param $db description d'une base de données (voir description)
 : @return liste d'éléments `<item/>`
 : @see https://docs.basex.org/main/JSON_Functions
 :)
declare function m:getDatabaseItems(
    $db as map(*)
    ) as element()* {
  let $text := file:read-text($db('url'))
  let $json := json:parse($text, map {
    'format':     map:get($db, 'conversion', 'direct'),
    'duplicates': map:get($db, 'duplicates', 'use-first')
    })
  for $item in $json/json/items/*
    return <item>{
      for $m in $item/metadata/*
        return <metadata pid="{$m/property/text()}">{$m/value/text()}</metadata>
    }</item>
};

(:~
 :)
declare function m:serialize(
    $items as element()*,
    $options as map(*)?
  ) as xs:string {
  let $content :=
  <json type="object" arrays="items metadata" objects="json _">
  <items>{
  for $item in $items
    return
    <_>{
      <metadata>{
      for $m in heimdall:getMetadata($item)
        return
        <_>{
          <property>{string($m/@pid)}</property>,
          <value>{$m/text()}</value>
        }</_>
      }</metadata>
    }</_>
  }</items>
  </json>
  return
  serialize($content, map {
    'method': 'json',
    'json': map {
    }
  })
};
