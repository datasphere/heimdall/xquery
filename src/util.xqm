module namespace m = 'http://heimdall.huma-num.fr/util';

(:~
 : Retourne la valeur correspondant à une clé `$key` dans une map `$db`.
 : <br>
 : Si `$key` n'existe pas, une valeur par défaut `$default` est retournée.
 : @param $db      map
 : @param $key     clé à rechercher au sein de `$db`
 : @param $default valeur par défaut, retournée si `$key` n'est pas trouvée
 : @return valeur correspondant à `$key` dans `$db`, ou `$default`
 :)
declare function m:get(
    $db as map(*),
    $key as xs:string,
    $default as xs:string
    ) as xs:string? {
  let $value := $db($key)
  return
  if (empty($value)) then $default
  else $value
};
