(:~
: Module d'interrogation générique des fichiers HML (=Heurist xML).
: @see https://gitlab.huma-num.fr/datasphere/heimdall
:)
module namespace m = 'http://heimdall.huma-num.fr/hml';

(:~
 : Liste tous les items présents dans la base de données `$db`.
 : Le retour est une liste d'éléments `<item/>`.
 : Chaque élément `<item/>` contient 0..N éléments `<metadata/>` :
 : ```
 : <item>
 :   <metadata id="id1">value1</metadata>
 :   <metadata id="id2">value2</metadata>
 :   ...
 :   <metadata id="idN">valueN</metadata>
 : </item>
 : ```
 : Le travail de cette fonction est d'aboutir à cette organisation en éléments
 : `<item/>` et `<metadata/>` en gommant les spécificités du format HML.
 : Le format HML n'est pas vraiment spécifié, mais pour résumer, il comprend
 : des éléments `<record/>`, eux-mêmes composés d'éléments `<detail/>` pour les
 : propriétées définies par l'utilisateur de la base de données d'origine,
 : ainsi que d'éléments spécifiques pour les métadonnées internes d'Heurist.
 : <br>
 : Le paramètre `$db` est une map décrivant la base de données au format HML
 : à analyser. Cette map doit obligatoirement contenir les clés :
 : <ul>
 : <li>`url` valuée avec un `xs:string` représentant l'URL de la base de données,
 :     c'est à dire le chemin local ou distant vers un fichier HML.
 : </li>
 : <li>`format` valuée à `xml:heurist`.</li>
 : <ul>
 : Si `$db("url")` désigne une base inexistante ou dans un format autre qu'HML,
 : le comportement de `getDatabaseItems` n'est pas spécifié.
 : @param $db description d'une base de données (voir description)
 : @return liste d'éléments `<item/>`
 : @see isHMLSpecificMetadata pour les tags correspondant à ces métadonnées internes
 :)
declare function m:getDatabaseItems(
    $db as map(*)
    ) as element()* {
  let $items := doc($db('url'))//*:record
  return
  for $item in $items
    return <item>{
      for $tag in $m:TAGS
        where $item/*[name()=$tag]   (: internal/reserved metadata :)
        return <metadata pid="{$tag}">{$item/*[name()=$tag]/text()}</metadata>,
      for $detail in $item/*:detail  (: user-defined metadata :)
        return <metadata pid="{$detail/@conceptID}">{$detail/text()}</metadata>
    }</item>
};

declare variable $m:TAGS := ('id', 'type', 'citeAs', 'title', 'added', 'modified', 'workgroup');

(:~
 : Permet de détecter si un identifiant de propriété `$propertyId` est
 : une métadonnée réservée d'Heurist ou non.
 : * Dans un fichier HML, les métadonnées reservées sont exprimées ainsi :
 :   `<$propertyId>value</$propertyId>`
 : * À l'inverse, toutes les autres métadonnées sont exprimées ainsi :
 :  `<detail conceptID="..." name="..." id="$propertyId" ...>value</detail>`
 : @param $propertyId identifiant de propriété (`<metadata pid="$propertyId">...`)
 : @return `true()` si $propertyId est réservée, `false()` sinon.
 :)
declare function m:isHMLSpecificMetadata(
    $propertyId as xs:string
    ) as xs:boolean {
  $m:TAGS = $propertyId
};



declare function m:serialize(
    $items as element()*,
    $options as map(*)?
  ) as xs:string {
  fn:error(
    xs:QName('TODO'),
    "Not implemented"
    )
};
