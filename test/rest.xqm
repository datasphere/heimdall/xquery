module namespace test = 'http://heimdall.huma-num.fr/tests';
import module namespace heimdall = 'http://heimdall.huma-num.fr' at '../src/heimdall.xqm';


declare %unit:test function test:read_api_nakala_endpoint_datas() {
  let $data := heimdall:getDatabaseItems(map {
    'url': 'https://api.nakala.fr/datas/10.34847%2Fnkl.f2dddawx',
(:    'url': 'https://api.nakala.fr/collections/10.34847%2Fnkl.85ebd28f/datas?page=1&amp;limit=10', :)
    'format': 'api:json'
    })
  return (
  (: empty object element in element <relations type="array"/> :)
  (: TODO: this maybe shouldn't create an empty <item/> element :)
  unit:assert-equals(empty($data[1]/*:metadata), true()),
  (: 3 object elements in element <files type="array"/> :)
  unit:assert-equals(test:metadata($data[2], 'name'), "Moc_66_d.jpg"),
  unit:assert-equals(test:metadata($data[3], 'name'), "Moc_66_da.jpg"),
  unit:assert-equals(test:metadata($data[4], 'name'), "Moc_66_db.jpg"),
  unit:assert-equals(test:metadata($data[2], 'mime__type'), "image/jpeg"),
  unit:assert-equals(test:metadata($data[3], 'mime__type'), "image/jpeg"),
  unit:assert-equals(test:metadata($data[4], 'mime__type'), "image/jpeg"),
  (: 9 object elements in element <metas type="array"/> :)
  unit:assert-equals(test:metadata($data[5], 'propertyUri'), "http://nakala.fr/terms#title"),
  unit:assert-equals(test:metadata($data[6], 'propertyUri'), "http://nakala.fr/terms#created"),
  unit:assert-equals(test:metadata($data[7], 'propertyUri'), "http://nakala.fr/terms#license"),
  unit:assert-equals(test:metadata($data[8], 'propertyUri'), "http://nakala.fr/terms#type"),
  unit:assert-equals(test:metadata($data[9], 'propertyUri'), "http://purl.org/dc/terms/description"),
  unit:assert-equals(test:metadata($data[10],'propertyUri'), "http://purl.org/dc/terms/publisher"),
  unit:assert-equals(test:metadata($data[11],'propertyUri'), "http://purl.org/dc/terms/contributor"),
  unit:assert-equals(test:metadata($data[12],'propertyUri'), "http://purl.org/dc/terms/issued"),
  unit:assert-equals(test:metadata($data[13],'propertyUri'), "http://nakala.fr/terms#creator"),
  (: 13 object elements in total :)
  unit:assert-equals(count($data), 13),
  unit:assert-equals(empty($data[14]), true())
  )
};

declare %unit:test function test:read_api_nakala_endpoint_collections() {
  let $data := heimdall:getDatabaseItems(map {
    'url': 'https://api.nakala.fr/collections/10.34847%2Fnkl.85ebd28f/datas?page=1&amp;limit=10',
    'format': 'api:json'
    })
  return
  for $item in $data
    return (
    unit:assert-equals(test:hasMetadata($item, 'version'), true()),
    unit:assert-equals(test:hasMetadata($item, 'files'), true()),
    unit:assert-equals(test:hasMetadata($item, 'relations'), true()),
    unit:assert-equals(test:metadata($item, 'status'), "published"),
    unit:assert-equals(test:hasMetadata($item, 'fileEmbargoed'), true()),
    unit:assert-equals(fn:starts-with(test:metadata($item, 'uri'), "https://doi.org/"), true()),
    unit:assert-equals(test:hasMetadata($item, 'identifier'), true()),
    unit:assert-equals(test:hasMetadata($item, 'metas'), true()),
    unit:assert-equals(test:hasMetadata($item, 'whatever'), false()),
    unit:assert-equals(count($item/*:metadata) >= 8, true()),
    unit:assert-equals(count($data), 10)
    )
};



declare function test:metadata(
    $item as element(),
    $metadata as xs:string
    ) as xs:string {
  string($item/*:metadata[@pid=$metadata]/text())
};

declare function test:hasMetadata(
    $item as element(),
    $metadata as xs:string
    ) as xs:boolean {
  fn:exists($item/*:metadata[@pid=$metadata])
};
