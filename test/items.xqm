module namespace test = 'http://heimdall.huma-num.fr/tests';
import module namespace heimdall = 'http://heimdall.huma-num.fr' at '../src/heimdall.xqm';

declare variable $test:DB as map(*) := map {
  'url': '../test/resources/misha_bahr_crocodile.xml',
  'format': 'xml:heurist'
  };
declare variable $test:ID as xs:string := '313643';



declare %unit:test function test:get_all_items() {
  let $items := heimdall:getDatabaseItems($test:DB)
  return unit:assert-equals(count($items), 755)
};



declare %unit:test function test:there_are_2_english_language_items() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $items := heimdall:getItems($given, '0-970', 'Anglais')
  return unit:assert-equals(count($items), 2)  (: 0-970=Langue (article) :)
};
declare %unit:test function test:there_is_1_german_language_item() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $items := heimdall:getItems($given, '0-970', 'Allemand')
  return unit:assert-equals(count($items), 1)  (: 0-970=Langue (article) :)
};
declare %unit:test function test:there_is_1_abstract_in_english() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $items := heimdall:getItems($given, '0-971', 'Anglais')
  return unit:assert-equals(count($items), 1)  (: 0-971=Langue (résumé) :)
};

declare %unit:test function test:there_is_1_item_with_this_title() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $items := heimdall:getItems($given, '2-1', 'CUIRASSE')
  return unit:assert-equals(count($items), 1)  (: 2-1=title :)
};
declare %unit:test function test:there_are_2_items_with_this_title() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $items := heimdall:getItems($given, '2-1', 'Sokrates and the crocodile')
  return unit:assert-equals(count($items), 2)  (: 2-1=title :)
};
declare %unit:test function test:there_are_no_item_with_this_title() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $items := heimdall:getItems($given, '2-1', 'NotATitleLOL')
  return unit:assert-equals(count($items), 0)  (: 2-1=title :)
};
declare %unit:test function test:empty_metadata_are_not_found() {
  let $given := heimdall:getDatabaseItems($test:DB)
  (: In $test:DB, record 713705 has empty <title/> tag, but it is not found :)
  let $items := heimdall:getItems($given, '2-1', '')
  return unit:assert-equals(count($items), 0)  (: 2-1=title :)
};

declare %unit:test function test:only_1_item_exists_for_this_id() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $items := heimdall:getItems($given, 'id', $test:ID)
  return unit:assert-equals(count($items), 1)
};
declare %unit:test function test:this_item_has_only_1_id() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItems($given, 'id', $test:ID)[1]
  let $id := $item/*:metadata[@pid='id']/text()
  return unit:assert-equals(count($id), 1)
};
declare %unit:test function test:this_item_has_correct_id() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItems($given, 'id', $test:ID)[1]
  let $id := $item/*:metadata[@pid='id']/text()[1]
  return unit:assert-equals(xs:string($id), $test:ID)
};
declare %unit:test function test:this_item_has_37_metadata() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItems($given, 'id', $test:ID)[1]
  let $metadata := $item/*:metadata
  return unit:assert-equals(count($metadata), 37)
};
declare %unit:test function test:this_other_item_has_6_metadata() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItems($given, 'id', '713705')[1]
  let $metadata := $item/*:metadata
  return unit:assert-equals(count($metadata), 6)
};
declare %unit:test function test:this_item_has_correct_number_of_sources() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItems($given, 'id', $test:ID)[1]
  let $metadata := $item/*:metadata[@pid='0-978']
  return unit:assert-equals(count($metadata), 5)  (: 0-978=Sources :)
};
declare %unit:test function test:this_item_has_correct_number_of_keywords() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItems($given, 'id', $test:ID)[1]
  let $metadata := $item/*:metadata[@pid='0-979']
  return unit:assert-equals(count($metadata), 12)  (: 0-979=Mots-clefs :)
};
declare %unit:test function test:this_item_does_not_have_this_metadata() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItems($given, 'id', $test:ID)[1]
  let $metadata := $item/*:metadata[@pid='NotAMetadataLOL']
  return unit:assert-equals(count($metadata), 0)
};
declare %unit:test function test:this_item_does_not_exist() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $items := heimdall:getItems($given, 'id', 'NotAnItemLOL')
  return unit:assert-equals(count($items), 0)
};



declare %unit:test function test:this_is_unique_id() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItem($given, '2-1', 'CUIRASSE')  (: 2-1=title :)
  let $expected := <metadata pid="2-1">CUIRASSE</metadata>
  return unit:assert-equals($item/*:metadata[@pid='2-1'], $expected)
};
declare %unit:test function test:this_is_not_unique_id() {
  (: @see https://www.w3.org/TR/2014/REC-xquery-30-20140408/#id-try-catch :)
  try {
    let $given := heimdall:getDatabaseItems($test:DB)
    let $error := heimdall:getItem($given, 'title', 'Sokrates and the crocodile')
    (: Ce test ÉCHOUE si heimdall:getItem n'échoue pas ... :)
    return unit:fail("This should never be reached, because previous line should fail.")
  }
  catch * {
    (: ... et à l'inverse, il réussit si la bonne erreur est levée. :)
    unit:assert-equals($err:code, xs:QName('XPTY0004'))
  }
};
declare %unit:test function test:this_is_missing_id() {
  let $given := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItem($given, 'title', 'NotATitleLOL')
  return unit:assert-equals(empty($item), true())
};
