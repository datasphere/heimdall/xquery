module namespace test = 'http://heimdall.huma-num.fr/tests';
import module namespace heimdall = 'http://heimdall.huma-num.fr' at '../src/heimdall.xqm';

declare variable $test:DB as map(*) := map {
  'url': '../test/resources/misha_bahr_crocodile.xml',
  'format': 'xml:heurist'
  };
declare variable $test:ID as xs:string := '313643';



declare %unit:test function test:this_item_has_37_metadata() {
  let $items := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItem($items, 'id', $test:ID)
  let $metadata := heimdall:getMetadata($item)
  return unit:assert-equals(count($metadata), 37)
};
declare %unit:test function test:this_item_has_12_keywords() {
  let $items := heimdall:getDatabaseItems($test:DB)
  let $item := heimdall:getItem($items, 'id', $test:ID)
  let $metadata := heimdall:getMetadata($item)  (: 0-979=Mots-clefs :)
  return unit:assert-equals(count($metadata[@pid='0-979']), 12)
};

declare %unit:test function test:empty_item_has_no_metadata() {
  let $returned := heimdall:getMetadata(<item></item>)
  return unit:assert-equals(empty($returned), true())
};
declare %unit:test function test:simplest_item_has_1_metadata() {
  let $metadata :=
  <placeholder>
    <metadata id='x' name='xxx'>valueX</metadata>
  </placeholder>
  let $given := <item>{$metadata/*:metadata}</item>
  let $returned := heimdall:getMetadata($given)
  let $expected := $metadata/*:metadata
  return unit:assert-equals($returned, $expected)
};
declare %unit:test function test:simple_item_has_3_metadata() {
  let $metadata :=
  <placeholder>
    <metadata id='x' name='xxx'>valueX</metadata>
    <metadata id='y' name='yyy'>valueY</metadata>
    <metadata id='z' name='zzz'>valueZ</metadata>
  </placeholder>
  let $given := <item>{$metadata/*:metadata}</item>
  let $returned := heimdall:getMetadata($given)
  let $expected := $metadata/*:metadata
  return unit:assert-equals($returned, $expected)
};
declare %unit:test function test:it_works_as_long_as_there_are_metadata() {
  let $given :=
  <placeholder>
    <whatever id='whatever'>value0</whatever>
    <metadata id='x'>valueX</metadata>
    <metadata id='y'>valueY</metadata>
    <metadata id='z'>valueZ</metadata>
  </placeholder>
  let $returned := heimdall:getMetadata($given)
  let $expected := $given/*:metadata
  return unit:assert-equals($returned, $expected)
};
declare %unit:test function test:it_performs_a_deep_search_on_any_object() {
  let $given :=
  <placeholder>
    <whatever id='whatever'>value0</whatever>
    <metadata id='x'>valueX</metadata>
    <subgroup>
      <metadata id='y'>valueY</metadata>
      <metadata id='z'>valueZ</metadata>
    </subgroup>
  </placeholder>
  let $expected :=
  <metadata>
    <metadata id='x'>valueX</metadata>
    <metadata id='y'>valueY</metadata>
    <metadata id='z'>valueZ</metadata>
  </metadata>
  let $returned := heimdall:getMetadata($given)
  return unit:assert-equals($returned, $expected/*:metadata)
};
declare %unit:test function test:it_just_returns_empty_on_any_object() {
  let $given :=
  <placeholder>
    <whatever id='whatever'>value0</whatever>
    <subgroup>
      <child id='child' />
    </subgroup>
  </placeholder>
  let $returned := heimdall:getMetadata($given)
  return unit:assert-equals(empty($returned), true())
};



declare %unit:test function test:test_get_empty_item_values() {
  let $given := <item></item>
  let $map := heimdall:getValues($given)
  return unit:assert-equals(map:size($map), 0)
};
declare %unit:test function test:test_get_flat_item_values() {
  let $given :=
  <item>
    <metadata pid='x'>valueX</metadata>
    <metadata pid='y'>valueY</metadata>
    <metadata pid='z'>valueZ</metadata>
  </item>
  let $map := heimdall:getValues($given)
  return (
    unit:assert-equals(map:contains($map, 'x'), true()),
    unit:assert-equals(map:contains($map, 'y'), true()),
    unit:assert-equals(map:contains($map, 'z'), true()),
    unit:assert-equals(xs:string(map:get($map, 'x')), 'valueX'),
    unit:assert-equals(xs:string(map:get($map, 'y')), 'valueY'),
    unit:assert-equals(xs:string(map:get($map, 'z')), 'valueZ'),
    unit:assert-equals(map:size($map), 3)
  )
};
declare %unit:test function test:test_get_deep_item_values() {
  let $given :=
  <item>
    <whatever pid='whatever'>value0</whatever>
    <metadata pid='x'>valueX</metadata>
    <subgroup>
      <metadata pid='y'>valueY</metadata>
      <metadata pid='z'>valueZ</metadata>
    </subgroup>
  </item>
  let $map := heimdall:getValues($given)
  return (
    unit:assert-equals(map:contains($map, 'x'), true()),
    unit:assert-equals(map:contains($map, 'y'), true()),
    unit:assert-equals(map:contains($map, 'z'), true()),
    unit:assert-equals(xs:string(map:get($map, 'x')), 'valueX'),
    unit:assert-equals(xs:string(map:get($map, 'y')), 'valueY'),
    unit:assert-equals(xs:string(map:get($map, 'z')), 'valueZ'),
    unit:assert-equals(map:size($map), 3)
  )
};



declare %unit:test function test:test_get_single_metadata_by_id() {
  let $item :=
  <item>
    <metadata pid='x'>valueX</metadata>
    <metadata pid='y'>valueY</metadata>
  </item>
  let $output := heimdall:getValueById($item, 'x')
  return unit:assert-equals($output, 'valueX')
};
declare %unit:test function test:test_get_multiple_metadata_by_id() {
  let $item :=
  <item>
    <metadata pid='x'>valueX</metadata>
    <metadata pid='y'>valueY</metadata>
    <metadata pid='y'>valueZ</metadata>
  </item>
  let $output := heimdall:getValueById($item, 'y')
  return (
    unit:assert-equals($output[1], 'valueY'),
    unit:assert-equals($output[2], 'valueZ')
  )
};
declare %unit:test function test:test_get_no_metadata_by_id() {
  let $item :=
  <item>
    <metadata pid='x'>valueX</metadata>
    <metadata pid='y'>valueY</metadata>
    <metadata pid='z'>valueZ</metadata>
  </item>
  let $output := heimdall:getValueById($item, 'a')
  return unit:assert-equals(empty($output), true())
};
declare %unit:test function test:test_get_no_metadata_by_non_existing_id() {
  let $item :=
  <item>
    <metadata pid='x'>valueX</metadata>
  </item>
  let $output := heimdall:getValueById($item, 'xxx')
  return unit:assert-equals(empty($output), true())
};



declare %unit:test function test:test_get_single_metadata_by_name() {
  let $item :=
  <item>
    <metadata pid='x' name='xxx'>valueX</metadata>
    <metadata pid='y' name='yyy'>valueY</metadata>
  </item>
  let $output := heimdall:getValueBy($item, 'name', 'xxx')
  return unit:assert-equals($output, 'valueX')
};
declare %unit:test function test:test_get_multiple_metadata_by_name() {
  let $item :=
  <item>
    <metadata pid='x' name='xxx'>valueX</metadata>
    <metadata pid='y' name='yyy'>valueY</metadata>
    <metadata pid='z' name='yyy'>valueZ</metadata>
  </item>
  let $output := heimdall:getValueBy($item, 'name', 'yyy')
  return (
    unit:assert-equals($output[1], 'valueY'),
    unit:assert-equals($output[2], 'valueZ')
  )
};
declare %unit:test function test:test_get_no_metadata_by_name() {
  let $item :=
  <item>
    <metadata pid='x' name='xxx'>valueX</metadata>
    <metadata pid='y' name='yyy'>valueY</metadata>
    <metadata pid='z' name='zzz'>valueZ</metadata>
  </item>
  let $output := heimdall:getValueBy($item, 'name', 'aaa')
  return unit:assert-equals(empty($output), true())
};
declare %unit:test function test:test_get_no_metadata_by_name_that_is_id() {
  let $item :=
  <item>
    <metadata pid='x' name='xxx'>valueX</metadata>
  </item>
  let $output := heimdall:getValueBy($item, 'name', 'x')
  return unit:assert-equals(empty($output), true())
};
