module namespace test = 'http://heimdall.huma-num.fr/tests';
import module namespace hml = 'http://heimdall.huma-num.fr/hml' at '../src/hml.xqm';



declare %unit:test function test:is_metadata_id() {
  unit:assert-equals(hml:isHMLSpecificMetadata('id'), true())
};
declare %unit:test function test:is_metadata_type() {
  unit:assert-equals(hml:isHMLSpecificMetadata('type'), true())
};
declare %unit:test function test:is_metadata_citeAs() {
  unit:assert-equals(hml:isHMLSpecificMetadata('citeAs'), true())
};
declare %unit:test function test:is_metadata_title() {
  unit:assert-equals(hml:isHMLSpecificMetadata('title'), true())
};
declare %unit:test function test:is_metadata_added() {
  unit:assert-equals(hml:isHMLSpecificMetadata('added'), true())
};
declare %unit:test function test:is_metadata_modified() {
  unit:assert-equals(hml:isHMLSpecificMetadata('modified'), true())
};
declare %unit:test function test:is_metadata_workgroup() {
  unit:assert-equals(hml:isHMLSpecificMetadata('workgroup'), true())
};

declare %unit:test function test:is_not_metadata_detail() {
  unit:assert-equals(hml:isHMLSpecificMetadata('detail'), false())
};
declare %unit:test function test:is_not_metadata_something_else() {
  unit:assert-equals(hml:isHMLSpecificMetadata('whatever'), false())
};
