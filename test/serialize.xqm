module namespace test = 'http://heimdall.huma-num.fr/tests';
import module namespace heimdall = 'http://heimdall.huma-num.fr' at '../src/heimdall.xqm';

declare variable $test:DB as map(*) := map {
  'url': './test/resources/pets.csv',
  'separator': ',',
  'format': 'csv'
  };
declare variable $test:NOT_SUPPORTED as xs:string := 'FODC0002';

declare %unit:test function test:csv2csv() {
  let $data := heimdall:getDatabaseItems($test:DB)
  let $file := heimdall:serialize($data, map {
    'format': 'csv',
    'header': ('id', 'name')
    })
  return (
    unit:assert-equals(count($data), 12),
    unit:assert-equals($file, $test:CSV)
  )
};

declare %unit:test function test:csv2hera() {
  let $data := heimdall:getDatabaseItems($test:DB)
  let $file := heimdall:serialize($data, map {
    'format': 'xml:hera',
    'root': 'polop'
    })
  return (
    unit:assert-equals(count($data), 12),
    unit:assert-equals($file, $test:XML)
  )
};

declare variable $test:CSV := "id,name
C1,Morris
C2,Morris
D1,Fido
D4,Mr Bowser
D8,Fido
C4,Lady Sheba
D5,Hubert
C10,Apples
G4,Nibbles
C11,Morris
D9,Winky
C6,Fido
";
declare variable $test:XML := "&lt;polop xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xsi:schemaLocation=&quot;https://gitlab.huma-num.fr/datasphere/hera/schema/schema.xsd&quot;&gt;&lt;items&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;C1&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Morris&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;6&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;cat&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;0-2&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;No&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;C2&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Morris&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;7&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;cat&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;5-8&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;Yes&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;D1&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Fido&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;15&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;dog&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;5-8&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;No&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;D4&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Mr Bowser&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;20&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;dog&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;3-4&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;No&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;D8&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Fido&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;40&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;dog&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;3-4&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;Yes&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;C4&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Lady Sheba&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;10&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;cat&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;5-8&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;No&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;D5&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Hubert&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;60&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;dog&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;0-2&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;No&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;C10&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Apples&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;17&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;cat&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;5-8&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;Yes&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;G4&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Nibbles&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;0.2&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;gerbil&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;0-2&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;NA&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;C11&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Morris&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;10&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;cat&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;5-8&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;No&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;D9&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Winky&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;11&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;dog&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;3-4&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;No&lt;/metadata&gt;&lt;/item&gt;&lt;item&gt;&lt;metadata pid=&quot;id&quot;&gt;C6&lt;/metadata&gt;&lt;metadata pid=&quot;name&quot;&gt;Fido&lt;/metadata&gt;&lt;metadata pid=&quot;weight&quot;&gt;6&lt;/metadata&gt;&lt;metadata pid=&quot;animal&quot;&gt;cat&lt;/metadata&gt;&lt;metadata pid=&quot;age&quot;&gt;5-8&lt;/metadata&gt;&lt;metadata pid=&quot;shots&quot;&gt;Yes&lt;/metadata&gt;&lt;/item&gt;&lt;/items&gt;&lt;/polop&gt;";
