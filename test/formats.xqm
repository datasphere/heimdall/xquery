module namespace test = 'http://heimdall.huma-num.fr/tests';
import module namespace heimdall = 'http://heimdall.huma-num.fr' at '../src/heimdall.xqm';

declare variable $test:DB as map(*) := map {
  'url': '../test/resources/misha_bahr_crocodile.xml',
  'format': 'xml:heurist'
  };
declare variable $test:NOT_SUPPORTED as xs:string := 'FODC0002';


declare %unit:test function test:getDatabaseItems_missing_format() {
  (: @see https://www.w3.org/TR/2014/REC-xquery-30-20140408/#id-try-catch :)
  try {
    let $given := map:entry('url', $test:DB('url'))
    let $error := heimdall:getDatabaseItems($given)
    (: Ce test ÉCHOUE si l'appel ci-dessus n'échoue pas ... :)
    return unit:fail("This should never be reached, because previous line should fail.")
  }
  catch * {
    (: ... et à l'inverse, il réussit si la bonne erreur est levée. :)
    unit:assert-equals($err:code, xs:QName($test:NOT_SUPPORTED))
  }
};
declare %unit:test function test:getDatabaseItems_unsupported_format() {
  (: @see https://www.w3.org/TR/2014/REC-xquery-30-20140408/#id-try-catch :)
  try {
    let $given := map:merge((
      map:entry('format', 'whatever'),
      map:entry('url', $test:DB('url'))
      ))
    let $error := heimdall:getDatabaseItems($given)
    (: Ce test ÉCHOUE si l'appel ci-dessus n'échoue pas ... :)
    return unit:fail("This should never be reached, because previous line should fail.")
  }
  catch * {
    (: ... et à l'inverse, il réussit si la bonne erreur est levée. :)
    unit:assert-equals($err:code, xs:QName($test:NOT_SUPPORTED))
  }
};
declare %unit:test function test:getDatabaseItems_empty_format() {
  (: @see https://www.w3.org/TR/2014/REC-xquery-30-20140408/#id-try-catch :)
  try {
    let $given := map:merge((
      map:entry('format', ()),
      map:entry('url', $test:DB('url'))
      ))
    let $error := heimdall:getDatabaseItems($given)
    (: Ce test ÉCHOUE si l'appel ci-dessus n'échoue pas ... :)
    return unit:fail("This should never be reached, because previous line should fail.")
  }
  catch * {
    (: ... et à l'inverse, il réussit si la bonne erreur est levée. :)
    unit:assert-equals($err:code, xs:QName($test:NOT_SUPPORTED))
  }
};
