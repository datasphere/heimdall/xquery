module namespace test = 'http://heimdall.huma-num.fr/tests';
import module namespace csv = 'http://heimdall.huma-num.fr/csv' at '../src/csv.xqm';
import module namespace m = 'http://heimdall.huma-num.fr' at '../src/heimdall.xqm';
import module namespace map = 'http://heimdall.huma-num.fr/util' at '../src/util.xqm';

declare %unit:test function test:get_value() {
  let $map := map {
    'line_separator': '\n',
    'separator': ';',
    'format': 'csv'
    }
  return (
  unit:assert-equals(map:get($map, 'line_separator', '\n'), '\n'),
  unit:assert-equals(map:get($map, 'separator', ','), ';'),
  unit:assert-equals(map:get($map, 'cell_delimiter', '"'), '"'),
  unit:assert-equals(map:get($map, 'format', 'whatever'), 'csv')
  )
};

declare %unit:test function test:read_csv() {
  let $data := m:getDatabaseItems(map {
    'url': './test/resources/pets.csv',
    'separator': ',',
    'format': 'csv'
    })
  return (
    unit:assert-equals(count($data), 12),
    for $item at $index in $data
      return (
      unit:assert-equals($item/*:metadata[pid='id']/text(), $test:CSV/csv/record[$index]/id/text()),
      unit:assert-equals($item/*:metadata[pid='name']/text(), $test:CSV/csv/record[$index]/name/text()),
      unit:assert-equals($item/*:metadata[pid='weight']/text(), $test:CSV/csv/record[$index]/weight/text()),
      unit:assert-equals($item/*:metadata[pid='animal']/text(), $test:CSV/csv/record[$index]/animal/text()),
      unit:assert-equals($item/*:metadata[pid='age']/text(), $test:CSV/csv/record[$index]/age/text()),
      unit:assert-equals($item/*:metadata[pid='shots']/text(), $test:CSV/csv/record[$index]/shots/text())
      )
  )
};

declare variable $test:CSV := 
              <csv>
                <record>
                  <id>C1</id>
                  <name>Morris</name>
                  <weight>6</weight>
                  <animal>cat</animal>
                  <age>0-2</age>
                  <shots>No</shots>
                </record>
                <record>
                  <id>C2</id>
                  <name>Morris</name>
                  <weight>7</weight>
                  <animal>cat</animal>
                  <age>5-8</age>
                  <shots>Yes</shots>
                </record>
                <record>
                  <id>D1</id>
                  <name>Fido</name>
                  <weight>15</weight>
                  <animal>dog</animal>
                  <age>5-8</age>
                  <shots>No</shots>
                </record>
                <record>
                  <id>D4</id>
                  <name>Mr Bowser</name>
                  <weight>20</weight>
                  <animal>dog</animal>
                  <age>3-4</age>
                  <shots>No</shots>
                </record>
                <record>
                  <id>D8</id>
                  <name>Fido</name>
                  <weight>40</weight>
                  <animal>dog</animal>
                  <age>3-4</age>
                  <shots>Yes</shots>
                </record>
                <record>
                  <id>C4</id>
                  <name>Lady Sheba</name>
                  <weight>10</weight>
                  <animal>cat</animal>
                  <age>5-8</age>
                  <shots>No</shots>
                </record>
                <record>
                  <id>D5</id>
                  <name>Hubert</name>
                  <weight>60</weight>
                  <animal>dog</animal>
                  <age>0-2</age>
                  <shots>No</shots>
                </record>
                <record>
                  <id>C10</id>
                  <name>Apples</name>
                  <weight>17</weight>
                  <animal>cat</animal>
                  <age>5-8</age>
                  <shots>Yes</shots>
                </record>
                <record>
                  <id>G4</id>
                  <name>Nibbles</name>
                  <weight>0.2</weight>
                  <animal>gerbil</animal>
                  <age>0-2</age>
                  <shots>NA</shots>
                </record>
                <record>
                  <id>C11</id>
                  <name>Morris</name>
                  <weight>10</weight>
                  <animal>cat</animal>
                  <age>5-8</age>
                  <shots>No</shots>
                </record>
                <record>
                  <id>D9</id>
                  <name>Winky</name>
                  <weight>11</weight>
                  <animal>dog</animal>
                  <age>3-4</age>
                  <shots>No</shots>
                </record>
                <record>
                  <id>C6</id>
                  <name>Fido</name>
                  <weight>6</weight>
                  <animal>cat</animal>
                  <age>5-8</age>
                  <shots>Yes</shots>
                </record>
              </csv>;
