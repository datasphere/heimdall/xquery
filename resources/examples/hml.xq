import module namespace heimdall = 'http://heimdall.huma-num.fr' at '../../src/heimdall.xqm';

let $db := map {
  'url': '../test/resources/misha_bahr_crocodile.xml',
  'format': 'xml:heurist'
  }
let $items := heimdall:getDatabaseItems($db)
return
heimdall:serialize($items, map {
  'format': 'xml:hera'
  })
