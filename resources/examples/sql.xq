import module namespace heimdall = 'http://heimdall.huma-num.fr' at '../../src/heimdall.xqm';


let $db := map {
  'url': 'mysql://localhost',
  'database': 'ckp',
  'tables': ('the_people'),
  'login': 'root',
  'password': 'password',
  'format': 'sql:mysql'
  }
let $items := heimdall:getDatabaseItems($db)
return
heimdall:serialize($items, map {
  'format': 'csv',
  'header': ('id', 'name', 'name_chn', 'female', 'birthyear', 'deathyear')
})
