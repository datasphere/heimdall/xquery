import module namespace heimdall = 'http://heimdall.huma-num.fr' at '../../src/heimdall.xqm';


let $db := map {
  'url': './test/resources/pets.csv',
  'separator': ',',
  'format': 'csv'
  }
let $items := heimdall:getDatabaseItems($db)
(: this outputs result as XML elements :)
(: return $items :)
(: this outputs result as XML file contents :)
return
heimdall:serialize($items, map {
  'format': 'xml:hera',
  'root': 'custom_root_tag'
  })
(: this outputs result as CSV :)
(:
return
heimdall:serialize($items, map {
  'format': 'csv',
  'header': ('id','name')
  })
:)
