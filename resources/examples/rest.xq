import module namespace heimdall = 'http://heimdall.huma-num.fr' at '../../src/heimdall.xqm';


let $db := map {
(:  'url': 'https://swapi.dev/api/people/', :)
(:  'url': 'https://www.narutodb.xyz/api/character?page=68&amp;limit=20', :)
  'url': 'https://api.nakala.fr/collections/10.34847%2Fnkl.85ebd28f/datas?page=1&amp;limit=10',
  'format': 'api:json'
  }
let $items := heimdall:getDatabaseItems($db)
return
heimdall:serialize($items, map {
  'format': 'xml:hera',
  'root': 'example'
})
