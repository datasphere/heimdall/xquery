import module namespace heimdall = 'http://heimdall.huma-num.fr' at '../../src/heimdall.xqm';


let $db := map {
  'url': './test/resources/DOGE.json',
  'format': 'json'
  }
let $items := heimdall:getDatabaseItems($db)
(: this outputs result as XML elements :)
(: return <items>{$items}</items> :)
(: this outputs result as JSON :)
return
heimdall:serialize($items, map {
  'format': 'json'
  })
