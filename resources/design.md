# Heimdall

Module écrit en [XQuery](https://fr.wikipedia.org/wiki/XQuery) pour l'interrogation générique des [fichiers HML](https://github.com/HeuristNetwork/heurist/blob/h6dev/documentation_and_templates/scheme_record.xsd) (=Heurist xML) exportés par le SGBD [Heurist](https://github.com/HeuristNetwork/heurist). 

## Introduction

L'objectif de ce module est de récupérer des informations issues d'une base de données. 
La base de données est décrite dans un format XML issu du SGBD [Heurist](https://github.com/HeuristNetwork/heurist).
Ce module n'est destiné qu'à lire les informations et non à les modifier.
Il est prévu pour être utilisé dans [BaseX](https://docs.basex.org) depuis des scripts [XQuery](https://fr.wikipedia.org/wiki/XQuery).


## Considerations générales

Les concepts entrant en jeu dans l'implémentation de ce module sont les suivants:

* **item** : un _item_ (_élément_) se compose d'une ou plusieurs _métadonnées_
* **metadata** : une _metadata_ (_métadonnée_) possède un _id_ (_identifiant_), un _name_ (_nom_)
* **values** : une _value_ (_valeur_) est un chaîne de caractères liée à un _item_ et à une _metadata_

Les appelations _item_, _metadata_, _value_ apparaitront dans l'interface publique du module.
Les appelations spécifiques à l'organisation de la base de données parsée ne devont pas être visibles lors de l'utilisation de ce module.

Par exemple, voici les correspondances entre chacun des concepts et leurs équivalents dans HML :

|Heimdall|Heurist|
|---|---|
|item|record|
|metadata|record/child::*| 
|value|data(record/child::*)|



### Contraintes et dépendances

Ce parseur doit pouvoir s'adapter au caractère local ou distant de la BDD.
* Une base de données distante doit être accessible via une URL.
* Une base de données locale doit être accessible via :
  * un chemin local
  * un identifiant de base de données relatif à BaseX

Le namespace du module est `http://heimdall.huma-num.fr/` avec le préfixe `heimdall:`.

Dans la mesure du posssible, le module doit être écrit en un seul fichier ̀̀`heimdall.xq`
Si le module nécessite des fonctions privées on pourra le découper en plusieurs fichiers (par exemple `heimdall.tools:`)

Tous les paramètres utilisés dans les fonctions listées ci-desssous sont sensibles à la casse.


### Proposition d'interface

Voici une liste non-limitative des fonctions à proposer dans ce module:

* [#09](../../../issues/9) `getDatabaseItems($db)` : renvoie une liste des éléments présents dans la base au format XML.
* ~[#13](../../../issues/13) `getDatabaseProperties($db)` : renvoie une liste de properties présentes dans la base au format XML.~
* [#10](../../../issues/10) `getItems($items, $metadata, $value)` : filtre une liste d'éléments d'après une condition sur leurs métadonnées.  
  Exemple d'utilisation : `getItems($items, 'status', 'valid')` liste tous les éléments `<item/>` qui ont la métadonnée nommée "status" valuée à "valid".
* [#12](../../../issues/12) `getItem($items, $metadata, $value)` : renvoie un seul (ou aucun) élément `<item/>`.  
   La fonction doit lever une exception si plus d'un élément correspondant à cet appel sont trouvés.  
   Exemple d'utilisation : `getItem($items, 'id', '1515')` renvoie le élément d'identifiant unique '1515', si celui-ci existe.
* [#14](../../../issues/14) `getMetadata($item)` : renvoie une liste des métadonnées présentes dans l'élément `<item/>` passé en paramètre.
* [#15](../../../issues/15) `getValues($item)` : renvoie une map (metadata: value).
* [#16](../../../issues/16) `getValueById($item, $metadata_id)` : renvoie une chaîne de caractères correspondant à la valeur d'une métadonnée d'un élément `<item/>` donné, d'après l'_id_ de cette métadonnée.
* [#17](../../../issues/17) `getValueByName($item, $metadata_name)` : renvoie une chaîne de caractères correspondant à la valeur d'une métadonnée d'un élément `<item/>` donné, d'après le _name_ de cette métadonnée.


### Contraintes de développement

[#07](../../../issues/7) 
[#08](../../../issues/8) 
Le module doit être écrit en XQuery et implémenté dans BaseX d'après la méthode décrite à la section "[Repository](https://docs.basex.org/wiki/Repository)" de BaseX.

## Périmètre

* Ce module est d'abord pensé pour un fonctionnement avec BaseX dans le cadre du module MaX et de son utilisation par Estrades.
  La compatibilité avec n'importe quelle implémentation de Xquery sera proposée dans une version ultérieure.
* Ce module est d'abord pensé pour parser des fichiers XML générés par Heurist (HML).
  La compatibilité avec d'autres sources de données sera proposée dans une version ultérieure.
