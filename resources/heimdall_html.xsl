<?xml version="1.0" encoding="UTF-8"?>
<!-- Feuille de transformation XSLT qui permet de transformer le fichier heimdall.xml en documentation heimdall.html.

La CSS est assez chargée car elle est une copie de celle retrouvée dans le wikipédia des modules de Basex (exemple ici: https://docs.basex.org/wiki/CSV_Module). Elle est probablement nettoyable.-->

<xsl:stylesheet	version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                xmlns:xqdoc="http://www.xqdoc.org/1.0"
                exclude-result-prefixes="tei xsl">

    <xsl:output method="html" encoding="utf-8" />

    <xsl:template match="/">
            <html lang="fr">
             <head>
                <meta charset="utf-8"/>
                <title>Documentation du module xQuery Heimdall</title>
		<link href="../resources/xqdoc.css" rel="stylesheet" type="text/css"/>
            </head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject mw-editable page-CSV_Module rootpage-CSV_Module skin-vector action-view">
    <div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Heimdall Module</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From PHun Documentation</div>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output">
		<p><xsl:apply-templates select=".//xqdoc:module//xqdoc:description"/></p>

<!-- création d'une table des matières pour circuler dans le document -->

<div id="toc" class="toc"><div class="toctitle" lang="en" dir="ltr">
<h2>Table des matières</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Conventions">
    <span class="tocnumber">1</span><span class="toctext">Conventions</span></a>
</li>

<li class="toclevel-1 tocsection-2"><a href="#Functions">
    <span class="tocnumber">2</span> <span class="toctext">Fonctions</span></a>
<ul>

<!-- génération du sommaire des fonctions -->

<ul>
 <xsl:for-each select="//xqdoc:function">
    <li class="toclevel-2 tocsection-1">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="concat('#',./xqdoc:name)"/>
            </xsl:attribute>
            <span class="tocnumber"><xsl:value-of select="count(./preceding-sibling::xqdoc:function)+1"/></span>
            <span class="toctext"><xsl:value-of select="./xqdoc:name"/></span>
        </a>
    </li>
    </xsl:for-each>
</ul>

<!-- fin de génération -->


</ul>
</li>
</ul>
</div>

<!-- fin de la table des matières -->

<h1><span class="mw-headline" id="Conventions">Conventions</span></h1>
<p>Toutes les fonctions de ce module sont associés à l'espace de nom
<code><xsl:apply-templates select="//xqdoc:module//xqdoc:see"/></code>
qui est relié au préfixe <code>heimdall</code>.
</p>

<!-- DEBUT DE LA DESCRIPTION DES FONCTIONS :

Chaque fonction est décrite de la même manière suivant ce plan :

nom
signature
résumé
paramètres
un exemple
les erreurs s'il en existe

Exemple : 

heimdall:getDatabaseItems
Signature : heimdall:getDatabaseItems($db as map(*)) as element()*
Résumé : Liste tous les items présents dans la base de données $db. Le retour est une liste d'éléments item. Chaque élément item contient 0..N éléments metadata
Paramètres : dbmap(*)
Exemple
Erreur : `XPTY0004` s'il y a ambiguité (voir description)

-->

<h1><span class="mw-headline" id="Functions">Fonctions</span></h1>
<xsl:apply-templates select="//xqdoc:function"/>

</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http://docs.basex.org/index.php?title=CSV_Module&amp;oldid=16553">http://docs.basex.org/index.php?title=CSV_Module&amp;oldid=16553</a>"</div>

		
	</div>
</div>

<!-- FIN DE LA DESCRIPTION DES FONCTIONS -->

<!-- DÉBUT DU PARAGRAPHE SUR LES MODIFICATIONS ET LICENCE -->

<div id='footer' role='contentinfo'>
  <ul id='footer-info'>
    <li id='footer-info-lastmod'><xsl:text>Cette page a été modifiée pour la dernière fois le </xsl:text><span><xsl:apply-templates select=".//xqdoc:date"/></span></li>
    <li id='footer-info-copyright'>Ce logiciel est mis à disposition selon les termes de la <a href='https://www.gnu.org/licenses/agpl-3.0.fr.html'>licence publique générale GNU Affero</a>.</li>
  </ul>
  <div style="clear: both;"></div>
</div>

<!-- FIN DU PARAGRAPHE -->


<div style="display: none; font-size: 14.3px;" class="suggestions"><div class="suggestions-results"></div><div class="suggestions-special"></div></div></body>
</html>
</xsl:template>

<!-- STRUCTURATION DES FONCTIONS -->

<xsl:template match="xqdoc:function">
        <h2><span class="mw-headline">
            <xsl:attribute name="id">
                <xsl:value-of select="./xqdoc:name"/>
            </xsl:attribute>
            <xsl:value-of select="./xqdoc:name"/></span>
            </h2>
  <table width="100%">
    <tbody>
      <tr valign='top'> <!-- SIGNATURE -->
        <td width="120"><strong>Signature</strong></td>
        <td><pre><xsl:value-of select="substring-after(./xqdoc:signature, 'function ')"/></pre></td>
      </tr>
      <tr valign="top"> <!-- SUMMARY -->
        <td><strong>Résumé</strong></td>
        <td><xsl:apply-templates select="./xqdoc:comment/xqdoc:description"/></td>
      </tr>
      <tr valign="top"> <!-- PARAMETERS -->
        <td><strong>Paramètres</strong></td>
        <td><ul><xsl:apply-templates select=".//xqdoc:param"/></ul></td>
      </tr>
      <tr valign="top"> <!-- EXAMPLES -->
        <td><strong>Exemples</strong></td>
        <td><xsl:apply-templates select=".//xqdoc:custom"/></td>
      </tr>
      <xsl:if test=".//xqdoc:error"> <!-- ERRORS (IF ANY) -->
        <tr valign="top">
          <td><strong>Erreurs</strong></td>
          <td><xsl:apply-templates select=".//xqdoc:error"/></td>
        </tr>
      </xsl:if>
    </tbody>
  </table>
</xsl:template>

    <!-- APPLICATION DE RÈGLES POUR CHACUNE DES BALISES -->

    <xsl:template match="xqdoc:description | xqdoc:module | xqdoc:namespaces | xqdoc:error">
    <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="xqdoc:date">
        <xsl:apply-templates select="substring-before(., 'T')"/>
    </xsl:template>

    <xsl:template match="xqdoc:version">
        <dl>
            <dt><xsl:value-of select="concat('Version ' ,.)"/></dt>
        </dl>
    </xsl:template>	

    <xsl:template match="xqdoc:namespace">
        <li>
            <a>
                <xsl:attribute name="href">
                    <xsl:value-of select="./@uri"/>
                </xsl:attribute>
                <xsl:value-of select="./@prefix"/>
            </a>
        </li>
    </xsl:template>

    <xsl:template match="xqdoc:parameters">
        <ul><xsl:apply-templates/></ul>
    </xsl:template>

    <xsl:template match="xqdoc:parameter">
        <li><xsl:value-of select="./xqdoc:name"/>:<xsl:value-of select="./xqdoc:type"/></li>
    </xsl:template>

    <xsl:template match="xqdoc:param">
        <li><xsl:apply-templates/></li>
    </xsl:template>

    <!-- REPRISE DES BALISES HTML DÉJÀ PRÉSENTES DANS LE FICHIER HEIMDALL.XQM -->

    <xsl:template match="code">
      <code><xsl:apply-templates/></code>
    </xsl:template>

    <xsl:template match="pre">
      <pre><xsl:apply-templates/></pre>
    </xsl:template>

    <xsl:template match="br">
      <br><xsl:apply-templates/></br>
    </xsl:template>

    <xsl:template match="ul">
      <ul><xsl:apply-templates/></ul>
    </xsl:template>

    <xsl:template match="li">
      <li><xsl:apply-templates/></li>
    </xsl:template>

    <xsl:template match="a">
        <xsl:variable name="href">
            <xsl:value-of select="./@href"/>
        </xsl:variable>
        <a><xsl:attribute name="href"><xsl:value-of select="$href"/></xsl:attribute><xsl:apply-templates/></a></xsl:template>

</xsl:stylesheet>
