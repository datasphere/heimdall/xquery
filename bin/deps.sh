#!/usr/bin/env sh

apk update && apk upgrade
apk add openjdk11 bash   # install BaseX dependencies
echo "Installing BaseX..."
wget https://files.basex.org/releases/BaseX.zip
unzip BaseX.zip
