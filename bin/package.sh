#!/usr/bin/env sh

PACKAGE_NAME=${1:-heimdall.xar}

cp -R src heimdall
zip -r "$PACKAGE_NAME" LICENSE expath-pkg.xml heimdall
rm -rf heimdall
