#!/usr/bin/env sh

FILE=$1
MAVEN=https://repo1.maven.org/maven2
SAXON_VERSION=10.8
SAXON=Saxon-HE-$SAXON_VERSION.jar
BASEX=basex/lib

# generate documentation as an XML file (generation is inconsistent,
# regarding what it generates, so we need the rest of this script)
echo "Generating xqDoc..."
basex -c"<run file='./bin/doc.xq'/>" > heimdall.xml

# transforming `` in <code> tags, must be a better option
cat heimdall.xml | sed 's/`\([$a-zA-Z:\(\),"<\/>_\-]*\)`/<code>\1<\/code>/g' > temp
mv temp heimdall.xml

echo "Downloading dependencies..."
# download SAXON only if there is'nt a file named likely localy
# -P: download in directory different than current
[ -e "$BASEX/$SAXON" ] || wget $MAVEN/net/sf/saxon/Saxon-HE/$SAXON_VERSION/$SAXON -P $BASEX

echo "Generating HTML from xqDoc..."
java -jar $BASEX/$SAXON heimdall.xml ./resources/heimdall_html.xsl > $FILE
echo "Cleanup..."
# rm heimdall.xml
