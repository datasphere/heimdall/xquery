[_English presentation_](#presentation)  
[_Présentation en Français_](#présentation)  


# Presentation

![Heimdall banner](https://sharedocs.huma-num.fr/wl/?id=bTXaJHOJwSa2swWiqdHxHPbfqMouu38u&fmode=download)



## What is this?

Heimdall is a tool for converting more easily one or more databases from one format to another.  
It makes a technical no-brainer migration and interoperability between SQL databases ([MariaDB](https://mariadb.org/), [MySQL](https://www.mysql.com/)), [REST APIs](https://en.wikipedia.org/wiki/REST), and general-purpose exchange formats such as [CSV](https://wikipedia.org/wiki/Comma-separated_values) (used by [spreadsheets](https://www.libreoffice.org/discover/calc/) or [Omeka](https://omeka.org/s/)) or [XML](https://en.wikipedia.org/wiki/XML) (used by [Heurist](https://heuristnetwork.org/)).



## Why should I use it?

If, for example, you find yourself in one of the following situations...

- you need access to data, but it is not stored in a format compatible with your favorite software...
- you'd prefer to merge several heterogeneous databases into a single corpus, easier to analyze...
- you're considering which input format to use for your software or database...
- you want to make your data more accessible and interoperable...

... then Heimdall can offer you an exchange format, and abstract the details of data implementation: yours, but also those of others.

Thanks to Heimdall, you can also switch from one technology to another in a matter of moments, as and when you need to, without ever losing, disorganizing or corrupting your data.

In a nutshell, **HEIMDALL** is your **H**igh **E**nd **I**nteroperability **M**odule when **D**ata is **ALL** over the place.
It's a bridge between scattered islands of data.



## Is it any good?

Yes.  
Hope this helps?



## How can I use it?

Heimdall is available as an [XQuery](https://fr.wikipedia.org/wiki/XQuery) software package, shared under the terms of the [AGPL3+ license](https://www.gnu.org/licenses/agpl-3.0.en.html).  
The last (_nightly_) version is available at the following URL:  
https://datasphere.gitpages.huma-num.fr/heimdall/xquery/heimdall.xar

### BaseX install

If you have [BaseX installed](https://docs.basex.org/wiki/Startup), inside the user interface, you can [install the package](https://docs.basex.org/wiki/Repository#Installation) like this:
```
REPO INSTALL https://datasphere.gitpages.huma-num.fr/heimdall/xquery/heimdall.xar
```
If BaseX complains "_[repo:not-found] Package ... heimdall.xar not found._", you can temporarily set [`IGNORECERT=true`](https://docs.basex.org/wiki/Options#IGNORECERT) in your [BaseX configuration file](https://docs.basex.org/wiki/Configuration) and retry.

As an alternative, you can manually download the package and then install it using its local path. Using the command like interface, it can work like this:
```
curl -k https://datasphere.gitpages.huma-num.fr/heimdall/xquery/heimdall.xar --output heimdall.xar
basex -c"<repo-install path='heimdall.xar'/>"
```

### Past releases

Older versions can be downloaded [from this page](../../releases).

You can of course clone this repository using [git](https://git-scm.com/docs), too.



## Is it documented?

Yes, although the documentation is only available in French for now.  
[Here it is](https://datasphere.gitpages.huma-num.fr/heimdall/xquery/doc/heimdall.html).

All functions available in the library are defined in the `src/heimdal.xqm` module.  
The `resources/examples` folder show some real-life usage examples.  
Additionally, the `resources` folder holds some documentation drafts : [design document](../../blob/main/resources/design.md) and [preliminary architecture](../../blob/main/resources/heurix.drawio.svg).

You can of course clone this repository using [git](https://git-scm.com/docs), too.  
This will allow you to check the code and its documentation locally, and to [validate its quality](https://en.wikipedia.org/wiki/Software_quality), by running [XQuery unit tests](https://docs.basex.org/wiki/Unit_Module) with a command such as:
```
TEST test
```
Alternatively, you can check the current state of unit tests [here](https://datasphere.gitpages.huma-num.fr/heimdall/xquery/test/report.xml).

<br>

If you have any questions or comments, feel free to contact the team by [opening a ticket](../../issues/new).



## Are there any related repositories?

Yes !
* Project Estrades components are available [at this address](https://gitlab.huma-num.fr/estrades/)
* Project Datasphere components are available both :
  * [on the "general purpose" repository GitLab.com](https://gitlab.com/datasphere.science/), as well as
  * [on the repository hosted by the french research infrastucture Huma-Num](https://gitlab.huma-num.fr/datasphere/)



## And who are you?

Heimdall is the result of coordination between research support projects
[Estrades](https://estrades.hypotheses.org/)
and
[Datasphere](https://gitlab.com/datasphere.science/),
supported by the following french research institutions:
* [Maison Interuniversitaire des Sciences de l'Homme - Alsace (MISHA — UAR 3227)](https://www.misha.fr/)
* [Arts, civilisation et histoire de l'Europe (ARCHE — UR 3400)](https://arche.unistra.fr/)





<br><br><br><br><br>





# Présentation

![Bannière d'Heimdall](https://sharedocs.huma-num.fr/wl/?id=bTXaJHOJwSa2swWiqdHxHPbfqMouu38u&fmode=download)



## Quel est ce projet ?

Heimdall est un outil permettant de convertir facilement une ou plusieurs bases de données d'un format à un autre.  
Il rend plus facile la migration et l'interopérabilité entre les bases de données SQL ([MariaDB](https://mariadb.org/), [MySQL](https://www.mysql.com/fr/)), celles disposant d'une [API REST](https://fr.wikipedia.org/wiki/Representational_state_transfer), ou les systèmes de gestion de bases de données relationnelles permettant l'import/export de fichiers au format [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values) (tels que les [tableurs](https://www.libreoffice.org/discover/calc/) ou [Omeka](https://omeka.org/s/)) ou [XML](https://fr.wikipedia.org/wiki/Extensible_Markup_Language) (tels qu'[Heurist](https://heuristnetwork.org/)).



## Pourquoi devrais-je l'utiliser ?

Si vous êtes, par exemple, dans une des situations suivantes ...

- vous avez besoin d'accéder à des données, mais elle ne sont pas stockées dans un format compatible avec votre logiciel favori, ...
- vous préféreriez unifier plusieurs bases de données hétérogènes en un seul corpus plus facile à analyser, ...
- vous vous demandez quel format d'entrée privilégier pour votre logiciel de traitement ou votre base de données, ...
- vous désirez rendre vos données plus accessibles et interopérables, ...

... alors, Heimdall peut vous proposer un format d'échange, et abstraire les détails d'implémentation des données : les  votres, mais aussi celles des autres.

Grâce à Heimdall, vous pouvez aussi passer en quelques instants d'une technologie à l'autre au gré de vos besoins, sans jamais perdre, désorganiser ou corrompre vos données.

En résumé, **HEIMDALL** peut constituer un pont entre des îlots de données dispersées.



## Est-ce que c'est bien ?

Évidemment ! ʕᵔᴥᵔʔ



## Comment puis-je l'utiliser ?

Heimdall est disponible comme un paquet logiciel [XQuery](https://fr.wikipedia.org/wiki/XQuery), mis à disposition selon les termes de la [licence publique générale GNU Affero](https://www.gnu.org/licenses/agpl-3.0.fr.html).  
La dernière version (_nightly_) est disponible à l'adresse suivante :  
https://datasphere.gitpages.huma-num.fr/heimdall/xquery/heimdall.xar

### Installation avec BaseX

Si vous utilisez l'[interface graphique de BaseX](https://docs.basex.org/wiki/Startup),
vous pouvez [installer le paquet](https://docs.basex.org/wiki/Repository#Installation) comme ceci :
```
REPO INSTALL https://datasphere.gitpages.huma-num.fr/heimdall/xquery/heimdall.xar
```
Si BaseX vous renvoie l'erreur « _[repo:not-found] Package ... heimdall.xar not found._ », vous pouvez (temporairement) indiquer [`IGNORECERT=true`](https://docs.basex.org/wiki/Options#IGNORECERT) dans votre [ fichier de configuration](https://docs.basex.org/wiki/Configuration) et réessayer.

Alternativement, vous pouvez télécharger manuellement le paquet, puis l'installer grâce à son chemin local. En ligne de commande, cela peut être fait comme ceci :
```
curl -k https://datasphere.gitpages.huma-num.fr/heimdall/xquery/heimdall.xar --output heimdall.xar
basex -c"<repo-install path='heimdall.xar'/>"
```

### Versions antérieures

Les versions antérieures sont disponibles [sur cette page](../../releases).

Vous pouvez évidemment aussi cloner le présent dépôt en utilisant [git](https://git-scm.com/docs).



## Où est la documentation ?

[Vous pouvez la lire ici](https://datasphere.gitpages.huma-num.fr/heimdall/xquery/doc/heimdall.html).

L'interface publique de la libraire Heimdall est définie dans le module `src/heimdal.xqm`.  
Le dossier `resources/examples` documente quelques cas d'usage.  
Le dossier `resources` contient aussi la documentation préliminaire du projet : [spécification](../../blob/main/resources/design.md) et [schéma d'architecture](../../blob/main/resources/heurix.drawio.svg).

Vous pouvez évidemment aussi cloner le présent dépôt en utilisant [git](https://git-scm.com/docs).  
Cela vous permettra de parcourir directement le code ainsi que sa documentation, et d'en valider vous-même la [qualité](https://fr.wikipedia.org/wiki/Qualit%C3%A9_logicielle), en lancant les [tests unitaires XQuery](https://docs.basex.org/wiki/Unit_Module) avec une commande comme :
```
TEST test
```
Vous pouvez aussi consulter directement en ligne [le dernier rapport de tests](https://datasphere.gitpages.huma-num.fr/heimdall/xquery/test/report.xml).

<br>

Si vous avez la moindre question ou remarque, n'hésitez pas à contacter l'équipe en [ouvrant un ticket](../../issues/new).



## Y a t'il d'autres projets liés à celui-ci ?

Oui !
* Les différents composants du projet Estrades sont disponibles [à cette adresse](https://gitlab.huma-num.fr/estrades/)
* Les différents composants du projet Datasphere sont disponibles à la fois :
  * [sur la forge « grand public » GitLab.com](https://gitlab.com/datasphere.science/), ainsi que
  * [sur la forge institutionnelle de l'infrastructure de recherche Huma-Num](https://gitlab.huma-num.fr/datasphere/)



## Et qui êtes-vous ?

Heimdall est le fruit de la coordination entre les projets de soutien à la recherche
[Estrades](https://estrades.hypotheses.org/)
et
[Datasphere](https://gitlab.huma-num.fr/datasphere/),
portés par la
[Maison Interuniversitaire des Sciences de l'Homme - Alsace (MISHA — UAR 3227)](https://www.misha.fr/)
et le laboratoire
[Arts, civilisation et histoire de l'Europe (ARCHE — UR 3400)](https://arche.unistra.fr/)
.
